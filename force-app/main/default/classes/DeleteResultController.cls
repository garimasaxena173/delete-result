public with sharing class DeleteResultController {
    
    public static List<Account> getDeletedRecordIds(String inputString){

        String input = String.escapeSingleQuotes(inputString);//Entered by User
    
        Account[] accts = [SELECT Id from Account WHERE Name LIKE :input];

        // Delete the accounts
        Database.DeleteResult[] drList = Database.delete(accts, false);

        for(Database.DeleteResult dr : drList) {
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted account with ID: ' + dr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : dr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                }
            }
        }
        return drList;
    }
}
